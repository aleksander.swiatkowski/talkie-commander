type Bot = {
  id: string;
  name: string;
};

const createPath = (path: string) => `/api/${path}`;
const get = (path: string) => async () => {
  const response = await fetch(path, {
    credentials: 'include',
  });
  const bots = await response.json();
  return bots;
};

export const TalkieService = {
  bots: {
    get: get(createPath('me/bots')),
  },
};
