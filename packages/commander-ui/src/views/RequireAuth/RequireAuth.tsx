import { Navigate, useLocation } from 'react-router';

import { useAuth } from '../../hooks/useAuth';

export const RequireAuth: React.FC = ({ children }) => {
  let auth = useAuth();
  let location = useLocation();

  return !auth?.isSignedIn ? (
    <Navigate to="/login" state={{ from: location }} replace />
  ) : (
    <>{children}</>
  );
};
