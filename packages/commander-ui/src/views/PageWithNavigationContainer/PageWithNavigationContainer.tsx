import { PageWithSidebar } from '@talkie/component-library';

import { useAuth } from '../../hooks/useAuth';
import { useBots } from '../../hooks/useBots';

export const PageWithNavigationContainer: React.FC = () => {
  const auth = useAuth();
  const { bots } = useBots();

  return (
    <PageWithSidebar email={auth?.user?.email} groups={[]} bots={bots ?? []}>
      Test
    </PageWithSidebar>
  );
};
