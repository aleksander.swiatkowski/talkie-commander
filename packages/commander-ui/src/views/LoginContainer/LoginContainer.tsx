import { useNavigate } from 'react-router';

import { LoginPage } from '@talkie/component-library';
import { useForm } from '@talkie/core';

import { useAuth } from '../../hooks/useAuth';

export const LoginContainer: React.FC = () => {
  const navigate = useNavigate();
  const auth = useAuth();
  const { fieldsProps, onSubmit, isSubmitting } = useForm({
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true);
      await auth?.signIn(values.login, values.password);
      navigate('/');
      setSubmitting(false);
    },
    initialFields: {
      login: {
        name: 'login',
        label: 'Login',
        placeholder: 'Login',
      },
      password: {
        name: 'password',
        label: 'Password',
        placeholder: 'Password',
      },
      rememberMe: {
        name: 'rememberMe',
        label: 'Remember me',
      },
    },
  });
  return (
    <LoginPage
      fieldsProps={fieldsProps}
      isLoading={isSubmitting}
      onSubmit={onSubmit}
    />
  );
};
