export const awsconfig = {
  Auth: {
    identityPoolId: 'eu-central-1:944e29ff-ce35-4fd1-855c-c4c46a19888c', // REQUIRED - Amazon Cognito Identity Pool ID
    region: 'eu-central-1', // REQUIRED - Amazon Cognito Region
    userPoolId: import.meta.env.VITE_USER_POOL_ID, // OPTIONAL - Amazon Cognito User Pool ID
    userPoolWebClientId: import.meta.env.VITE_USER_POOL_CLIENT_ID, // OPTIONAL - Amazon Cognito Web Client ID,
    cookieStorage: {
      // REQUIRED - Cookie domain (only required if cookieStorage is provided)
      domain: import.meta.env.VITE_COOKIE_DOMAIN || 'localhost',
      // OPTIONAL - Cookie expiration in days
      expires: 7,
      // OPTIONAL - Cookie secure flag
      // Either true or false, indicating if the cookie transmission requires a secure protocol (https).
      secure: !!import.meta.env.VITE_COOKIE_DOMAIN,
    },
  },
};
