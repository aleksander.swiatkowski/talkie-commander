import { Auth } from 'aws-amplify';
import React, { createContext, useContext, useEffect, useState } from 'react';

type CognitoUser = {
  email: string;
  username: string;
  userId: string;
  accessToken: string;
};

const AuthContext = createContext<
  ReturnType<typeof useProvideAuth> | undefined
>(undefined);
export const useAuth = () => useContext(AuthContext);

export const ProvideAuth: React.FC = ({ children }) => {
  const auth = useProvideAuth();
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
};

export const useProvideAuth = () => {
  const [user, setUser] = useState<CognitoUser | null>(null);
  const [isSignedIn, setIsSignedIn] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    if (!user) {
      setIsLoading(true);
      Auth.currentSession()
        .then((session) => {
          const { getIdToken, getAccessToken } = session;

          const idToken = getIdToken();
          const accessToken = getAccessToken();

          const user = {
            email: idToken.payload.email,
            username: idToken.payload.preferred_username,
            userId: idToken.payload.sub,
            accessToken: accessToken.getJwtToken(),
          };

          setIsSignedIn(true);
          setUser(user);
        })
        .catch((err) => {
          setHasError(true);
        })
        .finally(() => {
          setIsLoading(false);
        });
    }
  }, [user]);

  const signIn = (email: string, password: string) =>
    Auth.signIn(email, password).then((cognitoUser) => {
      const {
        attributes,
        signInUserSession: { accessToken },
      } = cognitoUser;

      const user = {
        email: attributes.email,
        username: attributes.preferred_username,
        userId: attributes.sub,
        accessToken: accessToken.jwtToken,
      };

      setIsSignedIn(true);
      setUser(user);

      return user;
    });

  const signOut = () =>
    Auth.signOut().then(() => {
      setIsSignedIn(false);
      setUser(null);
    });

  return {
    user,
    isSignedIn,
    isLoading,
    hasError,
    signIn,
    signOut,
  };
};
