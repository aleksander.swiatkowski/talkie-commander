import { useQuery } from 'react-query';

import { TalkieService } from '../services/TalkieService';

export const useBots = () => {
  const query = useQuery('bots', TalkieService.bots.get);

  return {
    bots: query.data,
    isLoading: query.isLoading,
    error: query.error,
  };
};
