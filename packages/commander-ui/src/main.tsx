import { CssBaseline, ThemeProvider } from '@mui/material';
import { Amplify } from 'aws-amplify';
import React from 'react';
import ReactDOM from 'react-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import { BrowserRouter as Router } from 'react-router-dom';

import { theme } from '@talkie/component-library';

import { App } from './App';
import { awsconfig } from './aws-exports';
import { ProvideAuth } from './hooks/useAuth';
import './index.scss';

Amplify.configure(awsconfig);
const queryClient = new QueryClient();

ReactDOM.render(
  <React.StrictMode>
    <CssBaseline />
    <ThemeProvider theme={theme}>
      <QueryClientProvider client={queryClient}>
        <Router>
          <ProvideAuth>
            <App />
          </ProvideAuth>
        </Router>
      </QueryClientProvider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
