import { Route, Routes } from 'react-router';

import { SiteRoot } from '@talkie/component-library';

import './App.module.scss';
import { LoginContainer } from './views/LoginContainer/LoginContainer';
import { PageWithNavigationContainer } from './views/PageWithNavigationContainer/PageWithNavigationContainer';
import { RequireAuth } from './views/RequireAuth/RequireAuth';

export const App: React.FC = () => {
  return (
    <SiteRoot>
      <Routes>
        <Route path="/login" element={<LoginContainer />} />
        <Route
          index
          element={
            <RequireAuth>
              <PageWithNavigationContainer />
            </RequireAuth>
          }
        />
      </Routes>
    </SiteRoot>
  );
};
