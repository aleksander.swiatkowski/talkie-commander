import react from '@vitejs/plugin-react';
import { defineConfig } from 'vite';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  define: {
    global: {},
  },
  server: {
    proxy: {
      '/api': {
        target: 'https://analytics-dev.talkie.ai',
        changeOrigin: true,
        secure: false,
        ws: true,
      },
    },
  },
});
