import { FormikConfig, FormikValues, useFormik } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';

import { FieldsPropsType, InitialFormFields } from './types';

type UseFormArgs<Values extends FormikValues, Fields extends string> = {
  onSubmit: FormikConfig<Values>['onSubmit'];
  initialFields: InitialFormFields<Fields>;
  validationSchema?: Yup.AnySchema;
  enableReinitialize?: boolean;
  validateOnBlur?: boolean;
  validateOnSubmit?: boolean;
};

export const useForm = <Values extends FormikValues, Fields extends string>({
  onSubmit,
  initialFields,
  validationSchema,
  enableReinitialize = false,
  validateOnBlur = true,
  validateOnSubmit = false,
}: UseFormArgs<Values, Fields>) => {
  const formik = useFormik({
    initialValues: _.chain(initialFields)
      .map((field, key) => [key, field.initialValue ?? ''])
      .fromPairs()
      .value() as Values,
    onSubmit,
    validationSchema,
    enableReinitialize,
    validateOnBlur: validateOnBlur && !validateOnSubmit,
    validateOnChange: !validateOnBlur && !validateOnSubmit,
  });

  return {
    onSubmit: (e: React.FormEvent) => {
      e.preventDefault();
      return formik.handleSubmit();
    },
    onFormSubmit: (e: React.FormEvent) => {
      e.preventDefault();
      formik.handleSubmit();
    },
    isSubmitting: formik.isSubmitting,
    hasErrors: !formik.isValid,
    setFieldError: formik.setFieldError,
    status: formik.status,
    fieldsProps: _.chain(initialFields)
      .map((field, key) => [
        key,
        {
          name: field.name,
          dataTestId: field.name,
          label: field.label,
          placeholder: field.placeholder,
          value: formik.values[key],
          error: Boolean(formik.errors[key]),
          helperText: formik.errors[key],
          onChange: formik.handleChange,
          onBlur: formik.handleBlur,
        },
      ])
      .fromPairs()
      .value() as FieldsPropsType<keyof typeof initialFields>,
  };
};

export type UseFormProps = ReturnType<typeof useForm>;
