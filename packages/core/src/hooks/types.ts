import { useForm } from './useForm';

export type FormValue = string | number | boolean;
export type SyntheticEventHandler = (event: React.SyntheticEvent) => void;
export type FieldsPropsType<Fields extends string> = {
  [k in Fields]: {
    name: string;
    label: string;
    placeholder?: string;
    value: FormValue;
    error?: boolean;
    helperText?: string;
    onChange:
      | React.ChangeEventHandler<HTMLInputElement>
      | SyntheticEventHandler;
    onBlur: React.FocusEventHandler;
  };
};

export type FormProps = Partial<ReturnType<typeof useForm>>;

export type InitialFormField = {
  name: string;
  label?: string;
  placeholder?: string;
  initialValue?: FormValue;
};

export type InitialFormFields<Fields extends string> = {
  [k in Fields]: InitialFormField;
};
