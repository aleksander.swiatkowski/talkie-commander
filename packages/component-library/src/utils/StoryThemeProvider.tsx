import { ThemeProvider } from '@mui/material';
import React from 'react';

import { theme } from '../theme';

export const StoryThemeProvider: React.FC = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);
