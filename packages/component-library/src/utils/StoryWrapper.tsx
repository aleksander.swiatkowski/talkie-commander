import { SiteRoot } from '..';

export const StoryWrapper: React.FC = ({ children }) => (
  <SiteRoot>{children}</SiteRoot>
);
