import { Paper } from '@mui/material';

import TalkieLogo from '../../assets/talkie-logo.svg';
import styles from './Logo.module.scss';

export const Logo: React.FC = () => (
  <Paper className={styles.logo} elevation={0}>
    <img src={TalkieLogo} />
  </Paper>
);
