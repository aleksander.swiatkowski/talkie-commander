import {
  Alert,
  Box,
  Button,
  Checkbox,
  CircularProgress,
  FormControlLabel,
  Paper,
  TextField,
  Typography,
} from '@mui/material';
import cx from 'classnames';
import React from 'react';

import { Page } from '../..';
import styles from './LoginPage.module.scss';

type LoginPageProps = {
  isLoading?: boolean;
  fieldsProps: FieldsPropsType<'login' | 'password' | 'rememberMe'>;
  onSubmit: React.FormEventHandler;
  alert?: {
    message: string;
    severity: 'error' | 'warning' | 'info' | 'success';
  };
};

export const LoginPage: React.FC<LoginPageProps> = ({
  isLoading = false,
  fieldsProps,
  onSubmit,
  alert,
}) => (
  <Page className={styles['login-page']}>
    <Paper
      elevation={0}
      className={styles['login-form-box']}
      component="form"
      onSubmit={onSubmit}
    >
      {alert && <Alert severity={alert.severity}>{alert.message}</Alert>}
      <Typography variant="h4" className={styles['login-header']}>
        Login
      </Typography>
      <TextField
        variant="standard"
        disabled={isLoading}
        {...fieldsProps.login}
      />
      <TextField
        variant="standard"
        type="password"
        disabled={isLoading}
        {...fieldsProps.password}
      />
      <Box className={cx(styles['login-row'], styles['login-row-spaced'])}>
        <FormControlLabel
          control={<Checkbox />}
          disabled={isLoading}
          {...fieldsProps.rememberMe}
          onChange={fieldsProps.rememberMe.onChange as SyntheticEventHandler}
        />
        <Button variant="text" disabled={isLoading}>
          Forgot password?
        </Button>
      </Box>
      <Button type="submit" variant="contained">
        {isLoading ? <CircularProgress size={24} color="inherit" /> : `Log in`}
      </Button>
      <Box className={styles['login-row']}>
        <Typography variant="body1" className={styles['login-footer-text']}>
          Don't have an account?
        </Typography>
        <Button variant="text" disabled={isLoading}>
          Sign up
        </Button>
      </Box>
    </Paper>
  </Page>
);
