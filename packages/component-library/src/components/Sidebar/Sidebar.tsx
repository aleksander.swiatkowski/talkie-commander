import {
  Autocomplete,
  DrawerProps,
  MenuItem,
  Drawer as MuiDrawer,
  Paper,
  Select,
  TextField,
  styled,
} from '@mui/material';
import { useMemo } from 'react';

import { Logo } from '../Logo/Logo';
import styles from './Sidebar.module.scss';

type SidebarItem = {
  icon?: React.ReactElement;
  label?: string;
  href?: string;
  isSelected?: boolean;
};

type SidebarBot = {
  id: string;
  name: string;
};

type SidebarGroup = {
  label: string;
  items: SidebarItem[];
};

type SidebarProps = {
  groups: SidebarGroup[];
  bots: SidebarBot[];
};

const Drawer = styled(MuiDrawer)<DrawerProps>(({ theme }) => ({
  '& .MuiPaper-root': {
    backgroundColor: theme.palette.background.paper,
    color: theme.palette.background.default,
  },
  '& .MuiDivider-root': {
    backgroundColor: theme.palette.background.default,
    marginBottom: theme.spacing(2),
  },
}));

export const Sidebar: React.FC<SidebarProps> = ({ groups, bots }) => {
  const botsOptions = useMemo(() => {
    return bots.map((bot) => ({
      label: bot.name,
      value: bot.id,
    }));
  }, [bots]);
  return (
    <Drawer
      variant="permanent"
      classes={{
        root: styles['sidebar'],
        paper: styles['paper'],
      }}
    >
      <Paper elevation={0}>
        <Logo />

        {bots && bots.length > 0 && (
          <Autocomplete
            className={styles['autocomplete']}
            options={botsOptions}
            renderInput={(params) => <TextField {...params} label="Bot" />}
            disableClearable
          />
        )}
      </Paper>
    </Drawer>
  );
};
