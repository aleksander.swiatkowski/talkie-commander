import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import Logout from '@mui/icons-material/Logout';
import PersonIcon from '@mui/icons-material/Person';
import {
  AppBar,
  Avatar,
  Box,
  Divider,
  IconButton,
  MenuItem,
  MenuList,
  Paper,
  Popover,
  Toolbar,
  Tooltip,
  Typography,
} from '@mui/material';
import { useState } from 'react';

import styles from './Topbar.module.scss';

type TopbarProps = {
  email?: string;
  onLogout?: React.MouseEventHandler;
};

export const Topbar: React.FC<TopbarProps> = ({ email, onLogout }) => {
  const [isPopoverOpen, setPopoverOpen] = useState(false);
  const [popoverAnchorEl, setPopoverAnchorEl] = useState<null | HTMLElement>(
    null
  );

  const onPopoverOpen = (event: React.MouseEvent<HTMLElement>) => {
    setPopoverAnchorEl(event.currentTarget);
    setPopoverOpen(true);
  };

  const onPopoverClose = () => {
    setPopoverAnchorEl(null);
    setPopoverOpen(false);
  };

  return (
    <AppBar position="static">
      <Toolbar>
        <Box sx={{ flexGrow: 1 }} />
        <Box className={styles['user-info']}>
          <Tooltip title="Your profile">
            <IconButton onClick={onPopoverOpen}>
              <Avatar>
                <AccountCircleIcon />
              </Avatar>
            </IconButton>
          </Tooltip>
          <Popover
            open={isPopoverOpen}
            anchorEl={popoverAnchorEl}
            onClose={onPopoverClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
          >
            <Paper className={styles['user-popover']}>
              <Typography
                variant="body2"
                className={styles['user-popover-row']}
              >
                <PersonIcon />
                <strong>{email}</strong>
              </Typography>
              <Divider />
              <MenuList>
                <MenuItem
                  className={styles['popover-menu-button']}
                  onClick={onLogout}
                >
                  <Logout />
                  Log out
                </MenuItem>
              </MenuList>
            </Paper>
          </Popover>
        </Box>
      </Toolbar>
    </AppBar>
  );
};
