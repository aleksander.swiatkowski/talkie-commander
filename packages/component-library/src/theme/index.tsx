import { createTheme } from '@mui/material';

export const theme = createTheme({
  palette: {
    mode: 'dark',
    background: {
      default: '#272934',
      paper: '#1f2129',
    },
    primary: {
      main: '#3C38BC',
    },
    secondary: {
      main: '#ff8c2f',
    },
    text: {
      primary: '#fff',
    },
  },
});
