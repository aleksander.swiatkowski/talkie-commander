import { Box } from '@mui/material';
import cx from 'classnames';

import styles from './Page.module.scss';

type PageProps = {
  className?: string;
};

export const Page: React.FC<PageProps> = ({ children, className }) => {
  return <Box className={cx(styles['page'], className)}>{children}</Box>;
};
