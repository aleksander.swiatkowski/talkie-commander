import React from 'react';

import { Topbar } from '../../components';
import { Sidebar } from '../../components/Sidebar/Sidebar';
import { Page } from '../Page/Page';

type PageWithSidebarProps = React.ComponentProps<typeof Topbar> &
  React.ComponentProps<typeof Sidebar>;

export const PageWithSidebar: React.FC<PageWithSidebarProps> = ({
  children,
  groups,
  bots,
  ...topbarProps
}) => {
  return (
    <>
      <Topbar {...topbarProps} />
      <Page>
        <Sidebar groups={groups} bots={bots} />
        {children}
      </Page>
    </>
  );
};
