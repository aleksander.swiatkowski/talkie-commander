import { Paper } from '@mui/material';

import styles from './SiteRoot.module.scss';

export const SiteRoot: React.FC = ({ children }) => (
  <Paper elevation={1} className={styles['site-root']}>
    {children}
  </Paper>
);
