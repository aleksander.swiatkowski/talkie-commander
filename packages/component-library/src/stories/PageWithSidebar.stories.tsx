import { PageWithSidebar } from '..';

export default {
  title: 'Layouts/PageWithSidebar',
  component: PageWithSidebar,
};

export const EmptyPage = () => {
  return <PageWithSidebar bots={[]} groups={[]} />;
};

export const WithBots = () => {
  return (
    <PageWithSidebar
      groups={[]}
      bots={[
        { id: '1', name: 'Bot 1' },
        { id: '2', name: 'Bot 2' },
        { id: '3', name: 'Bot 3' },
        { id: '4', name: 'Bot 4' },
        { id: '5', name: 'Bot 5' },
      ]}
    />
  );
};
