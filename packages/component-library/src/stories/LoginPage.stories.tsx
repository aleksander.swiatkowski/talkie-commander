import { useForm } from '@talkie/core';

import { LoginPage } from '../components/LoginPage/LoginPage';

export default {
  title: 'Pages/Login/LoginPage',
  component: LoginPage,
};

const useProps = () => {
  return useForm({
    onSubmit: (values, { setSubmitting }) => {
      setSubmitting(true);
      setTimeout(() => {
        setSubmitting(false);
      }, 1000);
    },
    initialFields: {
      login: {
        name: 'login',
        label: 'Login',
        placeholder: 'Login',
      },
      password: {
        name: 'password',
        label: 'Password',
        placeholder: 'Password',
      },
      rememberMe: {
        name: 'rememberMe',
        label: 'Remember me',
      },
    },
  });
};

export const LoginPasswordPage = () => {
  const { fieldsProps, onSubmit, isSubmitting } = useProps();
  return (
    <LoginPage
      fieldsProps={fieldsProps}
      isLoading={isSubmitting}
      onSubmit={onSubmit}
    />
  );
};

export const WithError = () => {
  const { fieldsProps, onSubmit, isSubmitting } = useProps();
  return (
    <LoginPage
      alert={{
        severity: 'error',
        message: 'Invalid login or password',
      }}
      fieldsProps={fieldsProps}
      isLoading={isSubmitting}
      onSubmit={onSubmit}
    />
  );
};
