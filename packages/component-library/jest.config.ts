export default async () => ({
    setupFiles: ['<rootDir>/src/tests/setupTests.ts'],
    transform: {
      '^.+\\.(ts|tsx|js|jsx)$': 'ts-jest',
    },
  });
  